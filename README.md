The perfect hotel choice for both business and leisure travellers that offers stylish and contemporary accommodation including suites boasts modern architecture and design; alongside two restaurants, 24 hour gym, 4 meeting rooms and a destination rooftop terrace bar with a view of London's skyline. || 
Address: 9 Aldgate High Street, London, England EC3N 1AH, UK ||
Phone: +44 20 3805 1000
